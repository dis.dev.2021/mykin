import 'focus-visible';

import uiTouchPunch from './plugins/jquery.ui.touch-punch';
import lazyImages from './plugins/lazyImages';
import documentReady from './helpers/documentReady';
import fancybox from '@fancyapps/fancybox';
import Swiper from 'swiper';

documentReady(() => {
    lazyImages();

    $( ".root-three" ).draggable();

    $('.scale-plus').on('click', function(e) {
        e.preventDefault();
        let mapScale = parseFloat($('.scale').attr('data-scale'));
        if (mapScale < 1.5) {
            mapScale = mapScale + 0.1;
            $('.scale').attr('data-scale',mapScale);
            let mapScaleStyle   = 'scale(' + mapScale + ')';
            $('.tree').css('transform', mapScaleStyle);
        }
    });

    $('.scale-minus').on('click', function(e) {
        e.preventDefault();
        let mapScale = parseFloat($('.scale').attr('data-scale'));
        console.log(mapScale);
        if (mapScale > 0.4) {
            mapScale = mapScale - 0.1;
            $('.scale').attr('data-scale',mapScale);
            let mapScaleStyle   = 'scale(' + mapScale + ')';
            $('.tree').css('transform', mapScaleStyle);
        }
    });


    $('.tree__link').on('click', function(e) {
        e.preventDefault();

        $.fancybox.open({
            src  : '#user-info',
            type : 'inline',
            opts : {
                afterShow : function( instance, current ) {
                    const userSlider = new Swiper(".user-slider", {
                        slidesPerView: "auto",
                        spaceBetween: 11,
                    });

                    $('.user-slider-prev').on('click', function(e) {
                        e.preventDefault();
                        userSlider.slidePrev();
                    });


                    $('.user-slider-next').on('click', function(e) {
                        e.preventDefault();
                        userSlider.slideNext();
                    });
                },

            }
        });
    });

    $('.user-image').fancybox({
        thumbs: {
            autoStart: true,
            hideOnClose: true,
            parentEl: ".fancybox-container",
            axis: "y"
        },
    });
});

